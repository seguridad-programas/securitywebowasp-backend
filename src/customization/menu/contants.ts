import { RoutePathEnum } from "src/role/enums/route-path.enum";

// * add here routes for that users don't have role assigned
export const DEFAULT_ROUTES: RoutePathEnum[] = [RoutePathEnum.PROFILE];
