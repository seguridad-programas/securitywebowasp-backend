import { Field, InputType } from "@nestjs/graphql";
import { RoutePathEnum } from "../enums/route-path.enum";
import { Permission } from "../types/permission.type";

@InputType()
export class CreateRoleInput {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field(() => [RoutePathEnum], { defaultValue: [] })
  routes: RoutePathEnum[];

  @Field(() => RoutePathEnum, { nullable: true })
  homeRoute?: RoutePathEnum;

  @Field({ defaultValue: true })
  deletable: boolean;

  @Field({ defaultValue: true })
  editable: boolean;

  @Field(() => [Permission], { nullable: true })
  permissions?: Permission[];
}
