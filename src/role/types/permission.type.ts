import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { EntityEnum } from "../../casl/enums/entity.enum";

@ObjectType()
@InputType("RouteType")
export class Permission {
  @Field(() => EntityEnum)
  entity: EntityEnum;

  @Field()
  manage: boolean;

  @Field()
  create: boolean;

  @Field()
  read: boolean;

  @Field()
  update: boolean;

  @Field()
  delete: boolean;
}
