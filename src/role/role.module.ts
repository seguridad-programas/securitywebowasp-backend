import { Module } from "@nestjs/common";
import { CaslAbilityFactory } from "src/casl/casl-ability.factory";
import { RoleRepository } from "./role.repository";
import { RoleResolver } from "./role.resolver";
import { RoleService } from "./role.service";

@Module({
  providers: [RoleResolver, RoleService, RoleRepository, CaslAbilityFactory],
})
export class RoleModule {}
