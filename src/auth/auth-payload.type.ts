import { Field, ObjectType } from "@nestjs/graphql";
import { RoutePathEnum } from "../role/enums/route-path.enum";

@ObjectType()
export class AuthPayloadType {
  @Field()
  accessToken: string;

  @Field()
  refreshToken: string;

  @Field(() => RoutePathEnum, { nullable: true })
  homeRoute?: RoutePathEnum;
}
