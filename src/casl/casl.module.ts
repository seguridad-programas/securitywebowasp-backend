import { Module } from "@nestjs/common";
import { RoleRepository } from "src/role/role.repository";

@Module({
  providers: [RoleRepository],
})
export class CaslModule {}
