import { registerEnumType } from "@nestjs/graphql";

export enum ActionEnum {
  Manage = "manage",
  Create = "create",
  Read = "read",
  Update = "update",
  Delete = "delete",
}

registerEnumType(ActionEnum, {
  name: "ActionEnum",
  description: "ActionEnum enum type",
});
