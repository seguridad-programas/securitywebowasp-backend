import { registerEnumType } from "@nestjs/graphql";

export enum EntityEnum {
  USER = "USER",
  ROLE = "ROLE",
  VENDOR = "VENDOR",
}

registerEnumType(EntityEnum, {
  name: "EntityEnum",
  description: "EntityEnum enum type",
});
