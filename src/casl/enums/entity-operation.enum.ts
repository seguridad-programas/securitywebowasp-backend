import { registerEnumType } from "@nestjs/graphql";

export enum EntityOperationEnum {
  CREATE = "CREATE",
  READ = "READ",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
}

registerEnumType(EntityOperationEnum, {
  name: "EntityOperationEnum",
  description: "EntityOperationEnum enum type",
});
