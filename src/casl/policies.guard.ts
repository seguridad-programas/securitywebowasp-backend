import {
  CanActivate,
  ExecutionContext,
  Injectable,
  SetMetadata,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { GqlExecutionContext } from "@nestjs/graphql";
import { EntityEnum } from "src/casl/enums/entity.enum";
import { CaslAbilityFactory } from "./casl-ability.factory";
import { EntityOperationEnum } from "./enums/entity-operation.enum";

export const EntityType = (entity: EntityEnum) => SetMetadata("entity", entity);
export const EntityOperation = (operation: EntityOperationEnum) =>
  SetMetadata("operation", operation);

@Injectable()
export class PoliciesGuard implements CanActivate {
  constructor(
    private caslAbilityFactory: CaslAbilityFactory,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const entity = this.reflector.get<EntityEnum>("entity", context.getClass());

    const overrideEntity = this.reflector.get<EntityEnum>(
      "entity",
      context.getHandler(),
    );

    const operation = this.reflector.get<EntityOperationEnum>(
      "operation",
      context.getHandler(),
    );

    const ctx = GqlExecutionContext.create(context);
    const req = ctx.getContext().req;

    return this.caslAbilityFactory.validateCanActivate(
      req.user,
      overrideEntity || entity,
      operation,
    );
  }
}
