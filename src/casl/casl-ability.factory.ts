import { Injectable } from "@nestjs/common";
import { Permission } from "src/role/types/permission.type";
import { User } from "src/user/schemas/user.schema";
import { EntityOperationEnum } from "./enums/entity-operation.enum";
import { EntityEnum } from "./enums/entity.enum";

const operationName = {
  [EntityOperationEnum.CREATE]: "create",
  [EntityOperationEnum.READ]: "read",
  [EntityOperationEnum.UPDATE]: "update",
  [EntityOperationEnum.DELETE]: "delete",
};

@Injectable()
export class CaslAbilityFactory {
  async validateCanActivate(
    user: User,
    entityType: EntityEnum,
    operation: EntityOperationEnum,
  ): Promise<boolean> {
    if (user.root) return true;

    return user.permissions.some(
      (permission: Permission) =>
        permission.entity === entityType &&
        permission[operationName[operation]],
    );
  }
}
