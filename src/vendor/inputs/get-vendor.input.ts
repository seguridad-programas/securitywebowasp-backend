import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class GetVendorsInput {
  @Field({ defaultValue: 10 })
  limit: number;

  @Field({ defaultValue: 0 })
  skip: number;

  @Field({ nullable: true })
  name?: string;
}
