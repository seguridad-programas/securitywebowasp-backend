import { Field, InputType } from "@nestjs/graphql";
import { Permission } from "src/role/types/permission.type";

@InputType()
export class CreateVendorUserInput {
  @Field()
  name: string;

  @Field()
  lastname: string;

  @Field()
  email: string;

  @Field()
  password: string;

  @Field(() => [String], { nullable: true })
  roles?: string[];

  @Field(() => [Permission], { nullable: true })
  permissions?: Permission[];

  @Field({ nullable: true })
  enableCustomPermissions?: boolean;
}
