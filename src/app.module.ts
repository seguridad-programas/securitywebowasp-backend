import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { GraphQLModule } from "@nestjs/graphql";
import { MongooseModule } from "@nestjs/mongoose";
import { HeaderResolver, I18nJsonParser, I18nModule } from "nestjs-i18n";
import * as path from "path";
import { AuthModule } from "./auth/auth.module";
import { SharedModule } from "./shared/shared.module";
import { UserModule } from "./user/user.module";
import { VendorModule } from "./vendor/vendor.module";
import { RoleModule } from './role/role.module';
import { CaslModule } from './casl/casl.module';
import { SqlapiModule } from './sqlapi/sqlapi.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      autoSchemaFile: "./schema.gql",
      playground: process.env.NODE_ENV !== "production",
      introspection: process.env.NODE_ENV !== "production",
      driver: ApolloDriver,
      context: ({ req, connection }) =>
        connection ? { req: connection.context } : { req },
      subscriptions: {
        "graphql-ws": {
          path: "/subscriptions",
          onClose: (_connection, reason) => {
            console.debug("closed", reason);
          },
          onNext: _data => {
            console.debug("next", _data);
          },
          onSubscribe: _data => {
            console.debug("subscribe", _data);
          },
          onConnect: _connectionParams => {
            console.debug("connect", _connectionParams);
          },
          onDisconnect: _connectionParams => {
            console.debug("disconnect", _connectionParams);
          },
        },
      },
      installSubscriptionHandlers: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>("NODRIZE_DATABASE_URL"),
        useFindAndModify: false,
      }),
      inject: [ConfigService],
    }),
    I18nModule.forRoot({
      fallbackLanguage: "es",
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, "/i18n/"),
        watch: true,
      },
      resolvers: [new HeaderResolver(["x-custom-lang"])],
    }),
    UserModule,
    VendorModule,
    AuthModule,
    SharedModule,
    RoleModule,
    CaslModule,
    SqlapiModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
