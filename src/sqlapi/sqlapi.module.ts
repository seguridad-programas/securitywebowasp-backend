import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { SqlapiController } from "./sqlapi.controller";
import { SqlapiService } from "./sqlapi.service";

@Module({
  providers: [SqlapiService],
  imports: [HttpModule],
  controllers: [SqlapiController],
})
export class SqlapiModule {}
