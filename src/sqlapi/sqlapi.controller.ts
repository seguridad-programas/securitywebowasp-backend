import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { RequestDto } from "./request.dto";
import { SqlapiService } from "./sqlapi.service";

@Controller("sqlapi")
export class SqlapiController {
  constructor(private sqlservice: SqlapiService) {}
  @Get()
  findAll(): any {
    return this.sqlservice.getTokenAuthPaf();
  }

  @Get("/task")
  findTask(): any {
    return this.sqlservice.getTaskNew();
  }

  @Post("/scan")
  async scanInyection(@Body() url: RequestDto): Promise<any> {
    console.log(url.url);
    return this.sqlservice.getScanSql(url.url);
  }

  @Post("/vulnerability")
  async scanVulnerability(@Body() body: RequestDto): Promise<any> {
    const { url } = body;
    return this.sqlservice.getScanVulnerability(url);
  }


  @Post("/vulnerabilitys")
  async scanVulnerabilitys(@Body() body: string): Promise<any> {
     console.log("body",body)
    return this.sqlservice.getScanVulnerability(body);
  }

  @Get("/data")
  getData(taskId: string): any {
    return this.sqlservice.getTaskData(taskId);
  }

  @Get("/logs/:taskid")
  getLogs(@Param('taskid') taskid: string): any {
   console.log(taskid);
    return this.sqlservice.getLogs(taskid);
  }
}
