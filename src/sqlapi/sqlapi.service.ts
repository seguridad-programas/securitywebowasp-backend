import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { lastValueFrom } from "rxjs";
import { ResponseDTO } from "./response.dto";
import { DataDTO } from "./data.dto";

@Injectable()
export class SqlapiService {
  constructor(private httpService: HttpService) {}

  private getUrlAuth(): string {
    return `http://127.0.0.1:8775`;
  }

  async getTokenAuthPaf(): Promise<string> {
    try {
      const response = await lastValueFrom(
        this.httpService.get(this.getUrlAuth() + "/version"),
      );

      return response.data;
    } catch (error) {
      console.log("Error: el el get ");
      return error;
    }
  }

  async getTaskNew(): Promise<string> {
    try {
      const response = await lastValueFrom(
        this.httpService.get(this.getUrlAuth() + "/task/new"),
      );

      return response.data;
    } catch (error) {
      console.log("Error: el el get ");
      return error;
    }
  }

 async getLogs(taskid:string): Promise<any> {
       
       try {
        const response =  await lastValueFrom(
          this.httpService.get(this.getUrlAuth()+"/scan/"+ taskid+"/log")
        );
        return response.data;
       }catch(error){
            console.log("Error get logs",error);
       }
  
 }

  async getTaskData(urls: string): Promise<DataDTO> {
    var taskId: string = urls;
    var ip : string ="Activa el parametro DB info -> Toda la información";
    var email : string = "No encontrado ";
    var privilegios : string = "No encontrado ";
    const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
  

    await delay(3000);
    try {
      
      const response = await lastValueFrom(
        this.httpService.get(this.getUrlAuth() + "/scan/" + taskId + "/data"),
      );
     
      console.log(await response.data.data.length)
      if(response.data.data.length != 0 ){
        const result = new Array();
        var dms : string =  "";
        var dmsver : string = "";
        var oss : string ="";

        var informationSchema = new Array();
        var acuart = new Array();
 
        response.data.data.forEach(numero => {
      
          if(numero.type == 1){
          
            numero.value.forEach(val => {
       
               dms=val.dbms; 
         
              dmsver=JSON.stringify(val.dbms_version);
              if(val.os === null){
                 oss="SO no encontrado"
              }else{
                oss=val.os;  
              }
              
         
              let claves = Object.keys(val.data)
              
              for(let i=0; i < claves.length; i++){
                let clave = claves[i]
                result.push(val.data[clave])

              }
              console.log(result);
            });
       
          }else if(numero.type == 13){
            
            const map1 = new Map();
            map1.set('informationSchema',numero.value.information_schema);
            map1.set('acuart',numero.value.acuart);
            informationSchema = numero.value.information_schema;
            acuart = numero.value.acuart;

          }else if(numero.type ==4 ){
     
              email = numero.value;
            
            
          }else if(numero.type == 6){
            ip = numero.value;
          }else if(numero.type == 10){
            //privilegios
            privilegios = JSON.stringify(numero.value);
          }
       
        }
         );
         return {
          datap: result,
          dbms: dms,
          dbmsversion : dmsver,
          os:oss,
          taskid: taskId,
          informationSchema : informationSchema,
          acuart: acuart,
          email : email,
          ip : ip
        };
      }else{
        console.log(await response.data);
        return {
          datap: new Array(),
          dbms: "No encontrado",
          dbmsversion : "No encontrado",
          os:"No encontrado",
          taskid: taskId,
          informationSchema : new Array(),
          acuart: new Array(),
          email : email,
          ip : ip
        };
      }

    } catch (error) {
      console.log("Error: el el get ");
      return error;
    }
  }

  async getScanVulnerability(url: string): Promise<any> {
    let response = await this.getScanSql(url);
    let taskId = response.task;

    const result = await this.getTaskData(taskId);

    return result;
  }

  async getScanSql(urlScan: string): Promise<ResponseDTO> {
    let datas = this.getTaskNew();
    console.log(await datas);
    let obj = JSON.stringify(await datas);
    const obj2 = JSON.parse(obj);
    let taskId: string = obj2.taskid;
  

    let resquete = JSON.parse(JSON.stringify(urlScan));
  
    try {

      let resquest = {
        url: urlScan,
      };


      const response = await lastValueFrom(
        this.httpService.post(
          this.getUrlAuth() + "/scan/" + taskId + "/start",
          resquete,
        ),
      );

      return {
        data: response,
        task: taskId,
      };
    } catch (error) {
      console.log("Error: el el post ");
      return error;
    }
  }
}
